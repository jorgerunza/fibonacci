
#jd.runza@uniandes.edu.co - Fibonnaci

#Def de variables del proceso:
anterior = -1
nuevo = 1
i=0
secuencia=''
sumatoria=0

#Inicio Interaccion:
serie = int(raw_input("Por favor escriba el numero de iteraciones  : "))

#Calculo
while  i <= serie:
    sum = anterior + nuevo
    sumatoria += sum
    secuencia = secuencia + str(sum) + ' , '
    anterior = nuevo
    nuevo = sum
    i += 1

#Respuesta:
print "La secuencia Fibonacci es: "+secuencia
print "La sumatoria es: "+str(sumatoria)